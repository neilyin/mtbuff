# ifndef __MT_BUFF_H__
# define __MT_BUFF_H__

/**
 * 回调用户函数：从缓冲区读到数据后要调用的数据处理操作
 * @param _pData 要写入的数据缓冲区
 * @param _nLen 数据的长度
 * @return 成功则返回0
 */
typedef int (*datapuller) (const char* _pData, unsigned _nLen);

/**
 * 初始化缓冲区，在调用其它函数前调用
 * @param _nBlockSize 创建的单个缓冲区大小
 * @param_nBlockNum 缓冲区个数
 */
void init_buff( unsigned int _nBlockSize, unsigned int _nBlockNum);

/**
 * 释放内存，在最后调用
 */
void uninit_buff();

/**
 * 向缓冲区中写手数据
 * @param _pData 要写入的数据
 * @param _nLen 数据长度
 * @return 成功则返回0
 */
int push_data( const char* _pData, unsigned long _nLen);

/**
 * 明确表示不再向缓冲区中写入数据
 * @return 成功则返回0
 */
int push_data_end();

/**
 * 从缓冲区中拉数据，拉到数据后调用用户的数据处理函数_fun
 * @param _fun 用户的数据处理函数
 * @return 如果返回-1，表示缓冲区中已经没有数据了，应不再调用此函数。成功则返回0。
 */
int pull_data( datapuller _fun);

# endif
