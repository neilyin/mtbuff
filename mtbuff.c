
#include <pthread.h>
#include <stdlib.h>
#include <string.h>

#include "mtbuff.h"


typedef struct _DBUFF
{
    pthread_rwlock_t rwlock;
    char* buf;
    unsigned int len;
    struct _DBUFF* next;
} DBUFF;

DBUFF* datas = 0;
DBUFF* buff_push = 0;
DBUFF* buff_pull = 0;

unsigned int BLOCK_LEN = 0;

void init_buff( unsigned int _nBlockSize, unsigned int _nBlockNum)
{
    DBUFF* buff = 0;
    datas = malloc(sizeof(DBUFF));
    buff_push = datas;
    buff_pull = datas;
    buff = datas;

    BLOCK_LEN = _nBlockSize;

    for (int i=0; i<_nBlockNum; i++)
    {
        pthread_rwlock_init( &buff->rwlock, 0);
        buff->len = 0;
        buff->buf = malloc(_nBlockSize);
        buff->next = (i<_nBlockNum-1)? malloc(sizeof(DBUFF)) : datas;
        buff = buff->next;
    }

    pthread_rwlock_wrlock(&buff_push->rwlock);
}

void uninit_buff()
{
    DBUFF* buff = datas;
    DBUFF* node = 0;

    while (buff)
    {
        node = buff;
        buff = buff->next;
        pthread_rwlock_destroy( &node->rwlock);
        free(node->buf);
        free(node);
    }
}

int push_data( const char* _pData, unsigned long _nLen)
{
    if (BLOCK_LEN - buff_push->len < _nLen)
    {
        pthread_rwlock_unlock(&buff_push->rwlock);
        buff_push = buff_push->next;
        pthread_rwlock_wrlock(&buff_push->rwlock);
        buff_push->len = 0;
        return push_data( _pData, _nLen);
    }

    memcpy( buff_push->buf + buff_push->len, _pData, _nLen);
    buff_push->len += _nLen;

    return 0;
}

int push_data_end()
{
    datas = buff_push->next;
    buff_push->next = 0;
    pthread_rwlock_unlock(&buff_push->rwlock);
    return 0;
}

int pull_data( datapuller _fun)
{
    if (!buff_pull)
        return 0;
    
    pthread_rwlock_rdlock(&buff_pull->rwlock);
    _fun( buff_pull->buf, buff_pull->len);
    pthread_rwlock_unlock(&buff_pull->rwlock);

    if (!buff_pull->next)
        return -1;
    
    buff_pull = buff_pull->next;
    return 0;
}
