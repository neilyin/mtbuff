
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mtbuff.h"

void* produce(void* _pObj)
{
    char buff[12000];
    size_t nReaded = 0;
    FILE* file = fopen( "/Users/neil/a.rar", "rb");
    
    while((nReaded = fread(buff, sizeof *buff, 12000, file)) > 0)
    {
       push_data( buff, nReaded);
    }
    
    push_data_end();
    return 0;
}

FILE* file2 = 0;

int print_data( const char* _szData, unsigned int _nLen)
{
    fwrite(_szData, sizeof *_szData, _nLen, file2);
    return 0;
}

void* consume(void *_pObj)
{
    file2 = fopen( "/Users/neil/b.rar", "wb");
    
    while( pull_data( print_data) == 0)
        ;

    return 0;
}

int main( int argc, char** argv)
{
    pthread_t tid1;
    pthread_t tid2;

    init_buff( 256*1024, 8);

    pthread_create(&tid1, NULL, produce, NULL);
    pthread_create(&tid2, NULL, consume, NULL);

    void *retVal;

    pthread_join(tid1, &retVal);
    pthread_join(tid2, &retVal);

    uninit_buff();
    return 0;
}
